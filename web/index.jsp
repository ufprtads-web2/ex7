<%-- 
    Document   : index
    Created on : Sep 18, 2018, 3:12:53 AM
    Author     : cassiano
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!doctype html>
<html>
    <head>
<%@ include file="templates/meta.html" %>  
<%@ include file="templates/header.html" %>  
    </head>
<%@ include file="templates/style.html" %>  
    <body>           
        <h3 style="color:red">
            <c:out value="${param.errorMsg}"/>
        </h3>        
    <form action="LoginServlet" method="POST">
        <div class="form-group">        
        <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Login" name="login">        
        </div>
        <div class="form-group">        
        <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Senha" name="senha">
        </div>  
        <button type="submit" class="btn btn-primary">Login</button>
    </form>        
    <%@ include file="templates/footer.jsp" %>      
    <%@include file="templates/js_scripts.html" %>        
    </body>
</html>
