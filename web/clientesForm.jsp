<%-- 
    Document   : clientesForm
    Created on : Oct 23, 2018, 3:30:17 AM
    Author     : cassiano
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${empty sessionScope.login}">
    <c:set var="errorMsg" value="Usuario deve se autenticar para acessar o sistema"></c:set>
    <jsp:forward page="/index.jsp">
        <jsp:param name="errorMsg" value="${errorMsg}"></jsp:param>
    </jsp:forward>
</c:if>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/templates/meta.html"%>
    <%@include file="/templates/header.html"%>
</head>
    <%@include file="/templates/style.html"%>
<body> 
    <%@include file="/templates/navbar.html"%>    
    <div class="w3-main" style="margin-left:340px;margin-right:40px">
    <c:if test="${not empty form}">
        <h1 class="w3-jumbo"><b>Alterar Clientes</b></h1>
        <h1 class="w3-xxxlarge w3-text-black"><b>Clientes</b></h1>
        <form action="/web2ex7/ClientesServlet?action=UPDATE" method="POST">        
            Nome:<br/><input type='text' name="nome" value=${cliente.nome}><br/>
            CPF:<br/><input type='text' name="cpf" value=${cliente.cpf}><br/>
            Email:<br/><input type='text' name="email" value=${cliente.email}><br/>
            Rua:<br/><input type='text' name="rua" value=${cliente.rua}><br/>
            NR:<br/><input type='text' name="nr" value=${cliente.nr}><br/>
            CEP:<br/><input type='text' name="cep" value=${cliente.cep}><br/>
            ID:<br/><input type='text' name="id" readonly="readonly" value=${cliente.id}><br/>
            <select id="estado" name="Estado">
                <c:forEach var="es" items="${requestScope.estados}">
                    <option value="${es.id}">"${es.nome}"</option>                    
                </c:forEach>
            </select>
            <select id="cidade" name="Cidade">
                <option value="000">Escolha a Cidade</option>
            </select>
            <input type='submit' value='Alterar'/>
        </form>
    </c:if>
    <c:if test="${empty form}">
        <h1 class="w3-jumbo"><b>Novo Cliente</b></h1>
        <h1 class="w3-xxxlarge w3-text-black"><b>Clientes</b></h1>
        <form action="/web2ex7/ClientesServlet?action=NEW" method="POST">        
            Nome:<br/><input type='text' name="nome" value=''><br/>
            CPF:<br/><input type='text' name="cpf" value=''><br/>
            Email:<br/><input type='text' name="email" value=''><br/>
            Rua:<br/><input type='text' name="rua" value=''><br/>
            NR:<br/><input type='text' name="nr" value=''><br/>
            CEP:<br/><input type='text' name="cep" value=''><br/>
            <select id="estado" name="estado">
                <c:forEach var="es" items="${requestScope.estados}">
                    <option value="${es.id}">"${es.nome}"</option>                    
                </c:forEach>
            </select>
            <select id="cidade" name="cidade">
                <option  value="000">Escolha a Cidade</option>
            </select>            
            <input type='submit' value='Salvar'/>
        </form>
    </c:if>
    <form action="/web2ex7/ClientesServlet">
    <input type="submit" value="Retornar" />
    </form>    
    </div>
    <%@include file="/templates/footer.jsp"%>
    <%@include file="/templates/js_scripts.html"%>
    </body>
</html>
