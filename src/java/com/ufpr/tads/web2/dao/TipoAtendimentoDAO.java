/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufpr.tads.web2.dao;

import com.ufpr.tads.web2.beans.Estado;
import com.ufpr.tads.web2.beans.TipoAtendimento;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author cassiano
 */
public class TipoAtendimentoDAO {
    
    private Connection con;

    public TipoAtendimentoDAO() {
        this.con = ConnectionFactory.getConnection();
    }    
    
    public List<TipoAtendimento> selectTipoAtendimentos() throws SQLException{
        List<TipoAtendimento> resultados = new ArrayList<>();        
        String sql = "SELECT * FROM tipo_atendimento";
        PreparedStatement st = con.prepareStatement(sql);        
        ResultSet rs = st.executeQuery();
        while (rs.next()) {
            TipoAtendimento tipo = new TipoAtendimento();              
            tipo.setId(rs.getInt("id"));
            tipo.setNome(rs.getString("nome"));    
            resultados.add(tipo);
        }        
        return resultados;      
    }
    
    public TipoAtendimento getTipoAtendimento(String id) throws SQLException{
        
        String sql = "SELECT * FROM tipo_atendimento WHERE id=(?) limit 1";
        PreparedStatement st = con.prepareStatement(sql);  
        st.setString(1, id);
        ResultSet rs = st.executeQuery();
        TipoAtendimento tipo = new TipoAtendimento();
        while (rs.next()){
            tipo.setId(rs.getInt("id"));
            tipo.setNome(rs.getString("nome"));            
            return tipo;
        }
        return null;
    }    
}
