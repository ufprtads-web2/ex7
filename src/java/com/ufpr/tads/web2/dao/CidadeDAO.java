/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufpr.tads.web2.dao;

import com.ufpr.tads.web2.beans.Cidade;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author cassiano
 */
public class CidadeDAO {
    
    private Connection con;
    
    public CidadeDAO() {
        this.con = ConnectionFactory.getConnection();
    }
    
    public List<Cidade> selectCidades() throws SQLException{
        List<Cidade> resultados = new ArrayList<>();        
        String sql = "SELECT * FROM cidade";
        PreparedStatement st = con.prepareStatement(sql);        
        ResultSet rs = st.executeQuery();
        while (rs.next()) {
              Cidade cidade = new Cidade();  
              cidade.setId(rs.getInt("id"));
              cidade.setNome(rs.getString("nome"));
              cidade.setUf(rs.getString("uf"));              
              cidade.setEstado_id(rs.getInt("estado"));
              resultados.add(cidade);
        }        
        return resultados;        
    }
    
        public Cidade selectCidade(String id) throws SQLException{
    
        String sql = "SELECT * FROM cidade WHERE id=(?) limit 1";
        PreparedStatement st = con.prepareStatement(sql);  
        st.setString(1, id);
        ResultSet rs = st.executeQuery();
        Cidade cidade = new Cidade();
        while (rs.next()){
            cidade.setId(rs.getInt("id"));
            cidade.setNome(rs.getString("nome"));
            cidade.setUf(rs.getString("uf"));
            cidade.setEstado_id(rs.getInt("estado"));
            return cidade;
        }    
        return null;
    }
        
    public List<Cidade> selectCidadesPerState(int es_id) throws SQLException{
        List<Cidade> resultados = new ArrayList<>();        
        String sql = "SELECT * FROM cidade where estado=(?)";
        PreparedStatement st = con.prepareStatement(sql);
        st.setInt(1, es_id);
        ResultSet rs = st.executeQuery();
        while (rs.next()) {
              Cidade cidade = new Cidade();  
              cidade.setId(rs.getInt("id"));
              cidade.setNome(rs.getString("nome"));
              cidade.setUf(rs.getString("uf"));              
              cidade.setEstado_id(rs.getInt("estado"));
              resultados.add(cidade);
        }        
        return resultados;        
    
    }
    
    public void closeConnection() throws SQLException{
        con.close();        
    }
    
}
