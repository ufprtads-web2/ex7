package com.ufpr.tads.web2.dao;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {
    public static String status = "Not connected";
            
    public static Connection getConnection() {
        Connection connection = null;                
        try {
            String driverName = "com.mysql.jdbc.Driver";
            String serverName = "172.17.0.3";    //caminho do servidor do BD 
            String mydatabase ="ex7";        //nome do seu banco de dados 
            String url = "jdbc:mysql://" + serverName + "/" + mydatabase; 
            String username = "root";        //nome de um usuário de seu BD       
            String password = "root";      //sua senha de acesso
            Class.forName(driverName);  
            connection = DriverManager.getConnection(url, username, password);
            if (connection != null) { 
                status = ("STATUS--->Conectado com sucesso!"); 
            } else { 
                status = ("STATUS--->Não foi possivel realizar conexão"); 
            }             
        } catch (ClassNotFoundException | SQLException e) {
            throw new RuntimeException (e);
        }
        return connection;
    }
}

