/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufpr.tads.web2.servlets;

import com.ufpr.tads.web2.beans.Cliente;
import com.ufpr.tads.web2.beans.Estado;
import com.ufpr.tads.web2.beans.LoginBean;
import com.ufpr.tads.web2.dao.EstadoDAO;
import com.ufpr.tads.web2.facade.ClientesFacade;
import com.ufpr.tads.web2.facade.EstadosFacade;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author cassiano
 */
@WebServlet(name = "ClientesServlet", urlPatterns = {"/ClientesServlet"})
public class ClientesServlet extends HttpServlet {
    
    private String errorMsg = "Usuário deve se autenticar para acessar o sistema";
    private String id = null;
    private String action = null;
    private String forward_page = null;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {     
        RequestDispatcher rd = getServletContext().getRequestDispatcher("/index.jsp");        
        try {
            HttpSession session = request.getSession();
            LoginBean usuario = (LoginBean)session.getAttribute("login");        
            
            if (usuario == null){                
                request.setAttribute("errorMsg",errorMsg);        
                rd.forward(request, response);
            }
            
            action = (String) request.getParameter("action");            
            id = request.getParameter("id");
            Cliente cliente = new Cliente();
            
            if (action == null){
                action = "LIST";
            }
            
            switch(action) {
                case "LIST":
                    forward_page = "/clientesListar.jsp";
                    List<Cliente> resultado = ClientesFacade.listClients();                    
                    request.setAttribute("clientes", resultado);                    
                    break;
                case "SHOW":
                    forward_page = "/clientesVisualizar.jsp";
                    cliente = ClientesFacade.getClient(id);
                    if (cliente != null){                        
                        request.setAttribute("cliente", cliente);                        
                    }
                    break;
                case "FORMUPDATE":                    
                    forward_page = "/clientesForm.jsp";
                    cliente = ClientesFacade.getClient(id);
                    if (cliente != null){                                                
                        List<Estado> estados = EstadosFacade.listStates();                                               
                        request.setAttribute("cliente", cliente);
                        request.setAttribute("estados", estados);                        
                        request.setAttribute("form", true);
                    }                    
                    break;
                case "REMOVE":
                    forward_page = "/portal.jsp";
//                    forward_page = "/ClientesServlet";
                    ClientesFacade.deleteClient(id);                    
                    break;
                case "UPDATE":
                    forward_page = "/portal.jsp";
//                    forward_page = "/ClientesServlet";
                    if (id != null){
                        int id_cliente = Integer.parseInt(id);
                        cliente.setId(id_cliente);
                        cliente.setNome(request.getParameter("nome"));
                        cliente.setCpf(request.getParameter("cpf"));
                        cliente.setEmail(request.getParameter("email"));
                        int city_id = Integer.parseInt(request.getParameter("cidade"));
                        cliente.setId_cidade(city_id);                        
                        cliente.setCep(request.getParameter("cep"));
                        cliente.setRua(request.getParameter("rua"));
                        int nr = Integer.parseInt(request.getParameter("nr"));
                        cliente.setNr(nr); 
                        ClientesFacade.updateClient(cliente);                    
                    }
                    break;
                case "FORMNEW":
                    forward_page = "/clientesForm.jsp";
                    List<Estado> estados = EstadosFacade.listStates();                                               
                    request.setAttribute("estados", estados);                    
                    break;
                case "NEW":                    
                    forward_page = "/portal.jsp";
//                    forward_page = "/ClientesServlet";
                    int nr = Integer.parseInt(request.getParameter("nr"));
                    cliente.setNr(nr);       
                    cliente.setNome(request.getParameter("nome"));
                    cliente.setCpf(request.getParameter("cpf"));
                    cliente.setEmail(request.getParameter("email"));
                    int city_id = Integer.parseInt(request.getParameter("cidade"));
                    cliente.setId_cidade(city_id);   
                    cliente.setCep(request.getParameter("cep"));
                    cliente.setRua(request.getParameter("rua"));                    
                    ClientesFacade.addClient(cliente);
                    break;
            }            
        } catch (SQLException ex) {
            Logger.getLogger(ClientesServlet.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            rd = getServletContext().getRequestDispatcher(forward_page);
            rd.forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
