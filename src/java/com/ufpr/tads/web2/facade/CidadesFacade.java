/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufpr.tads.web2.facade;

import com.ufpr.tads.web2.beans.Cidade;
import com.ufpr.tads.web2.dao.CidadeDAO;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author cassiano
 */
public class CidadesFacade {
    
    static CidadeDAO dao = null;
    static Cidade cidade = null;
    
    public static List listCities() throws SQLException{        
        dao = new CidadeDAO();
        List<Cidade> cidades = new ArrayList<>();
        try {
            cidades = dao.selectCidades();
            return cidades;
        } catch (SQLException e) {
           Logger.getLogger(ClientesFacade.class.getName()).log(Level.SEVERE, null, e); 
        }finally {
            dao.closeConnection();    
        }       
        return null;
    }
    
    public static List listCitiesPerState(int es_id) throws SQLException{
        dao = new CidadeDAO();
        List<Cidade> cidades = new ArrayList<>();
        try {
            cidades = dao.selectCidadesPerState(es_id);
            return cidades;
        } catch (SQLException e) {
            Logger.getLogger(ClientesFacade.class.getName()).log(Level.SEVERE, null, e); 
        }finally {
            dao.closeConnection();            
        }        
        return null;
    
    }
}
